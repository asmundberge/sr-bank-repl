{ mkDerivation, aeson, base, bytestring, cassava, directory
, double-conversion, filepath, ghc, haskeline, http-conduit
, http-types, lib, mtl, open-browser, optparse-applicative, parsec
, relude, text, time, transformers, unordered-containers
, uri-encode, yaml
}:
mkDerivation {
  pname = "srbank";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base bytestring cassava directory double-conversion filepath
    ghc haskeline http-conduit http-types mtl open-browser
    optparse-applicative parsec relude text time transformers
    unordered-containers uri-encode yaml
  ];
  executableHaskellDepends = [ base relude ];
  testHaskellDepends = [ base relude ];
  homepage = "https://gitlab.com/asmundberge/sr-bank-repl#readme";
  license = lib.licenses.bsd3;
}
