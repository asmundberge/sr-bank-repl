# SR-Bank-repl

## export CSV files from your Sparebank 1 bank

This tool is a very basic repl that allows you to simplify your bookkeeping workflow by allowing you to fetch bank CSV files from the command line.

it expects a file "srbank.yaml" to be located in the directory where the command is run, that contain the following:

```
clientId: <your-personal-client-id>
clientSecret: <your-personal-client-secret>
callbackUrl: https://auth-helper.herokuapp.com

accounts:
  - alias: myaccount
    directory: import/bank/csv/
    accountKey: "<bank-account-id>"
  - alias: myotheraccount
    directory: import/otherbankaccount/csv/
    accountKey: "<bank-account-id-2>"
```

## Commands

There are currently 3 commands: `accounts`, `transactions` and `export`.

### Accounts

```
> accounts
1234567890 My account name        123.00 kr
```


### Transactions

```
> transactions myaccount
2020-02-01 Bank fees             0.03 kr
2020-02-03 Mortgage          43534.00 kr
```


### Export

```
> export 2020-04 myaccount
exporting import/bank/csv/2020/4.csv
> export 2020-05 *
exporting import/bank/csv/2020/5.csv
exporting import/otherbankaccount/csv/2020/5.csv
> 
```


