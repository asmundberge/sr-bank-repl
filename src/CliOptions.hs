module CliOptions
    ( CliOptions(..)
    , parser
    ) where

import Relude

import qualified Options.Applicative as Options

data CliOptions
    = Empty
    | Command Text

parser :: Options.Parser CliOptions
parser
    = Command
        <$> (Options.strOption
                ( Options.long "command"
                  <> Options.short 'c'
                  <> Options.help "Execute a single command, then exit" ) )
    <|> pure Empty
