{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE NamedFieldPuns             #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}

module Account
    ( Account(..)
    , Key(..)
    , pretty
    ) where

import Relude

import Data.Aeson (FromJSON(..), withObject, (.:))
import Money (Money)
import qualified Data.Csv as Csv
import qualified Data.Text as Text
import qualified Money 

newtype Key = Key { keyToText :: Text }
    deriving (Show, Csv.ToField, FromJSON)

data Account = Account
    { accountKey :: Key
    , accountNumber :: Text
    , name :: Text
    , balance :: Money
    , availableBalance :: Money
    , owner :: Text
    }
    deriving (Show, Generic)

instance FromJSON Account where
    parseJSON = withObject "account" $ \o -> do
        accountKey <- o .: "key"
        name <- o .: "name"
        accountNumber <- o .: "accountNumber"
        balance <- Money.nokFromDouble <$> o .: "availableBalance" <|> o .: "balance"
        availableBalance <- Money.nokFromDouble <$> o .: "availableBalance"
        ownerObj <- o .: "owner"
        owner <- ownerObj .: "name"
        return (Account {..})

instance Csv.ToNamedRecord Account
instance Csv.DefaultOrdered Account

pretty :: Account -> Text
pretty (Account {..}) =
    mconcat
        [ Text.justifyLeft 11 ' ' (Text.take 10 (Account.keyToText accountKey))
        , Text.justifyLeft 12 ' ' accountNumber
        , Text.justifyLeft 23 ' ' (Text.take 22 name)
        , Text.justifyRight 12 ' ' (Money.pretty balance)
        ]

