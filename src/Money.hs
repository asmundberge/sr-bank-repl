{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE NamedFieldPuns             #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}

module Money
    ( Money
    , pretty
    , nokFromDouble
    ) where

import Relude

import Data.Aeson (FromJSON)
import Data.Double.Conversion.Text (toFixed)
import qualified Data.ByteString.Char8 as BS
import qualified Data.Csv as Csv

data Money = Money
    { amount :: Double
    , currencyCode :: Text
    }
    deriving (Show, Generic)

{- {
      "amount": 0.000,
      "currencyCode": "NOK"
   } -}

instance FromJSON Money

instance Csv.ToField Money where
    toField (Money {..}) =
        if currencyCode == "NOK" then
            BS.pack (show amount)
        else
            error "Money have to be NOK!"

pretty :: Money -> Text
pretty (Money {..}) =
    if currencyCode == "NOK" then
        toFixed 2 amount <> " kr"
    else
        error "Money have to be NOK!"

nokFromDouble :: Double -> Money
nokFromDouble n = Money
    { amount = n
    , currencyCode = "NOK"
    }
