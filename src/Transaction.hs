{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE NamedFieldPuns             #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Transaction
    ( Transaction(..)
    , TransactionDay(..)
    , pretty
    ) where

import Relude

import Data.Aeson ((.:), (.:?), (.!=), FromJSON(..), withObject)
import Data.Csv (ToField(..), DefaultOrdered, ToNamedRecord)
import Data.Time (Day, showGregorian, utctDay)
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)
import Money (Money)
import qualified Data.ByteString.Char8 as BS
import qualified Data.Text as Text
import qualified Money

newtype TransactionDay = TransactionDay { getPlainDay :: Day }
    deriving (Show, Generic, FromJSON)

data Transaction = Transaction
    { date :: TransactionDay
    , amount :: Money
    , description :: Text
    , transactionType :: Maybe Text
    , transactionCode :: Maybe Text
    , remoteAccount :: Maybe Text
    }
    deriving (Show, Generic)

instance ToField TransactionDay where
    toField (TransactionDay day) = BS.pack $ showGregorian day

instance FromJSON Transaction where
    parseJSON = withObject "transaction" $ \o -> do
        date <- dateFromInt <$> o .: "date"
        amount <- Money.nokFromDouble <$> o .: "amount"
        description <- o .:? "description" .!= ""
        transactionType <- o .:? "transactionType"
        transactionCode <- o .:? "transactionCode"
        remoteAccount <- o .:? "remoteAccount"

        return Transaction{..}

instance ToNamedRecord Transaction
instance DefaultOrdered Transaction

dateFromInt :: Int -> TransactionDay
dateFromInt n =
  TransactionDay $
    utctDay $
    posixSecondsToUTCTime $
    fromIntegral (n `div` 1000)

pretty :: Transaction -> Text
pretty (Transaction {..}) =
    mconcat
        [ Text.justifyLeft 11 ' ' $ Text.pack $ showGregorian $ getPlainDay date
        , Text.justifyLeft 31 ' ' (Text.take 30 description)
        , Text.justifyRight 12 ' ' (Money.pretty amount)
        ]
