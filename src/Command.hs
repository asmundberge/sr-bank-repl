{-# LANGUAGE RecordWildCards            #-}

module Command
    ( Command
    , Definition(..)
    , Error(..)
    , Result(..)
    , lookup
    , printHelp
    , quit
    , run
    ) where

import Relude

import Repl (Repl)
import qualified Api
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import qualified Repl

data Error
    = ApiError Api.Error
    | ArgumentError Text
    deriving (Show)

data Result
    = Continue
    | Quit

data Definition = Definition
    { name :: Text
    , help :: Text
    , command :: Command
    , complete :: [ String ] -> String -> Repl.ReplMonad [ String ]
    }

type Command = [ Text ] -> ExceptT Error Repl Result

quit :: Command
quit = const $ return Quit

unknown :: Text -> Command
unknown cmd _ = do
    liftIO $ Text.putStrLn ("Unknown command " <> cmd <> ".")
    return Continue

commandHelp :: Definition -> Text
commandHelp (Definition {..}) = do
    Text.justifyLeft 30 ' ' name <> " - " <> help

printHelp :: [ Definition ] -> Command
printHelp cmds _ = do
    putStrLn "Supported commands:"
    putStrLn ""
    liftIO $ Text.putStrLn $ Text.justifyLeft 30 ' ' "quit" <> " - Quit the program"
    liftIO $ Text.putStrLn $ Text.justifyLeft 30 ' ' "help" <> " - Show this help message"
    _ <- mapM ( liftIO . Text.putStrLn . commandHelp ) cmds
    return Continue

lookup :: Text -> [ Definition ] -> Command
lookup commandName = maybe (unknown commandName) command . find ((== commandName) . name)

run :: Command -> [ Text ] -> Repl Result
run cmd args = do
    result <- runExceptT (cmd args)
    either (print >=> (const $ return Continue)) return result
    
