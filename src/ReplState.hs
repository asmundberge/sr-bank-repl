module ReplState
    ( ReplState(..)
    , initialState
    , setAccounts
    , setToken
    ) where

import Relude

import Account (Account)
import Data.Time (Day, getCurrentTime, utctDay)
import qualified Auth.Types as Auth

data ReplState = 
    ReplState
        { authToken :: Maybe Auth.AuthToken
        , today :: Day
        , accounts :: [ Account ]
        } deriving (Show)

initialState :: Maybe Auth.AuthToken -> IO ReplState
initialState token = do
    currentDate <- utctDay <$> getCurrentTime
    return $ ReplState token currentDate []

setToken :: Maybe Auth.AuthToken -> ReplState -> ReplState
setToken token oldState =
    oldState { authToken = token }

setAccounts :: [ Account ] -> ReplState -> ReplState
setAccounts newAccounts oldState =
    oldState { accounts = newAccounts }
