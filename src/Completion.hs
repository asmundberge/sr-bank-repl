{-# LANGUAGE NamedFieldPuns             #-}
{-# LANGUAGE RecordWildCards            #-}

module Completion
    ( completionFunc
    ) where

import Relude

import Repl (ReplMonad)
import qualified Data.Text as Text
import qualified System.Console.Haskeline.Completion as Haskeline
import qualified Command

completionFunc :: [ Command.Definition ] -> Haskeline.CompletionFunc ReplMonad
completionFunc commandDefs =
    Haskeline.completeWordWithPrev Nothing [ ' ' ] (complete commandDefs)

complete :: [ Command.Definition ] -> String -> String -> Repl.ReplMonad [ Haskeline.Completion ]
complete commandDefs prev current = do
    completions <- (case reverse ((reverse . Text.unpack) <$> words (toText prev)) of
            [] -> completeCommands commandDefs current
            ( commandName:commandArgs ) -> completeArguments commandDefs commandName commandArgs current
            )
    return $ makeCompletion current completions

commonPrefix :: String -> String -> String
commonPrefix "" _ = ""
commonPrefix _ "" = ""
commonPrefix (a:as) (b:bs) = 
    if a == b then
        a : (commonPrefix as bs)
    else
        ""

longestCommonPrefix :: [ String ] -> String
longestCommonPrefix [ ] = ""
longestCommonPrefix [ a ] = a
longestCommonPrefix ( a:b:rest ) = longestCommonPrefix (commonPrefix a b : rest)

makeCompletion :: String -> [ String ] -> [ Haskeline.Completion ]
makeCompletion partial candidates =
    let
        prefix = longestCommonPrefix candidates
        count = length candidates
    in
    if count == 1 || prefix == "" || prefix == partial then
        map (\a -> Haskeline.Completion a a True) candidates
    else
        [ Haskeline.Completion prefix prefix False ]

completeCommands :: [ Command.Definition ] -> String -> Repl.ReplMonad [ String ]
completeCommands commandDefs partial = do
    let commands = map (Text.unpack . Command.name) commandDefs
    return $ filter (isPrefixOf partial) commands

completeArguments :: [ Command.Definition ] -> String -> [ String ] -> String -> Repl.ReplMonad [ String ]
completeArguments commandDefs commandName commandArgs current = do
    let maybeCommandDefinition = find (\def -> Command.name def == Text.pack commandName) commandDefs
    maybe (return []) ((\completer -> completer commandArgs current) . Command.complete) maybeCommandDefinition
