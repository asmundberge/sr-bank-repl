{-# LANGUAGE DeriveGeneric              #-}

module Auth.File
    ( write
    , read
    ) where

import Relude

import Data.Aeson (FromJSON, ToJSON)
import System.Directory (getHomeDirectory)
import System.FilePath ((</>))
import qualified Auth.Types as Auth
import qualified Data.Yaml as Yaml

data AuthFile = AuthFile { authToken :: Auth.AuthToken }
    deriving (Show, Generic)

instance ToJSON AuthFile
instance FromJSON AuthFile

filename :: IO FilePath
filename = do
    home <- getHomeDirectory
    return (home </> ".srbank.auth.yaml")

write :: Auth.AuthToken -> IO ()
write code = do
    let f = AuthFile code
    fn <- filename
    Yaml.encodeFile fn f

read :: IO (Maybe Auth.AuthToken)
read = do
    fn <- filename
    authFile <- Yaml.decodeFileEither fn
    return $ fmap authToken (rightToMaybe authFile)
