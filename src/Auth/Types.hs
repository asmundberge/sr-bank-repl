{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE RecordWildCards   #-}

module Auth.Types
    ( AuthCode
    , AuthToken(..)
    , addAuthHeader
    ) where

import Relude

import Data.Aeson (FromJSON(..), ToJSON(..))
import qualified Network.HTTP.Simple as Http

data AuthToken = AuthToken
    { access_token :: Text
    , refresh_token :: Text
    }
    deriving (Show, Generic)

instance FromJSON AuthToken
instance ToJSON AuthToken

addAuthHeader :: AuthToken -> Http.Request -> Http.Request
addAuthHeader (AuthToken {..}) req =
    Http.addRequestHeader "Authorization" ("Bearer " <> encodeUtf8 access_token) req


type AuthCode = Text
