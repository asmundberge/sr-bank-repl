module Repl
    ( Repl
    , ReplMonad
    , getState
    , modifyState
    , readConfig
    , runRepl
    ) where

import Relude

import ConfigFile (ConfigFile)
import ReplState (ReplState)
import System.Console.Haskeline (InputT, runInputT, Settings)

type ReplMonad = ReaderT ConfigFile (StateT ReplState IO)

type Repl = InputT ReplMonad

runRepl :: Settings ReplMonad -> ConfigFile -> ReplState -> Repl a -> IO a
runRepl settings config initialState = evaluatingStateT initialState . usingReaderT config . runInputT settings

readConfig :: Repl ConfigFile
readConfig =
    lift ask

getState :: Repl ReplState
getState =
    lift get

modifyState :: (ReplState -> ReplState) -> Repl ()
modifyState =
    lift . modify
