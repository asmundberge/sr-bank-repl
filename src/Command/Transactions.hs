{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE NamedFieldPuns             #-}

module Command.Transactions
    ( definition
    ) where

import Relude

import Control.Monad.Trans.Except (throwE, withExceptT)
import Repl (Repl)
import ReplState (ReplState(..))
import qualified Account
import qualified AccountConfig
import qualified Api
import qualified Command
import qualified Command.Export
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import qualified Repl
import qualified Transaction

definition :: Command.Definition
definition = Command.Definition
    { name = "transactions"
    , help = "<account> - List all transactions for a given account"
    , command = command
    , complete = complete
    }

accountLookup :: Text -> Repl ( Maybe Account.Key )
accountLookup query = do
    configs <- Command.Export.accountConfigLookup query
    let configIds = AccountConfig.accountKey <$> configs
    (ReplState { accounts }) <- Repl.getState
    let isId account = query == Account.keyToText (Account.accountKey account)
    let isNumber account = query == Account.accountNumber account
    let stateIds = Account.accountKey <$> filter (\a -> isId a || isNumber a) accounts
    return $ viaNonEmpty head $ stateIds <> configIds

command :: Command.Command
command args =
    case args of
        [ accountKeyText ] -> do
            maybeAccountKey <- lift $ accountLookup accountKeyText
            case maybeAccountKey of
                Just accountKey -> do
                    transactions <- withExceptT Command.ApiError $ Api.getTransactions accountKey
                    _ <- mapM (liftIO . Text.putStrLn . Transaction.pretty) transactions
                    return Command.Continue
                Nothing ->
                    throwE ( Command.ArgumentError ("Unknown account " <> accountKeyText ) )
        _ ->
            throwE ( Command.ArgumentError "Usage: transactions <account>" )

complete :: [ String ] -> String -> Repl.ReplMonad [ String ]
complete [] current = do
    configs <- Command.Export.accountConfigsFromQuery ""
    let configIds = (Account.keyToText . AccountConfig.accountKey) <$> configs
    let configAliases = AccountConfig.alias <$> configs
    (ReplState { accounts }) <- get
    let stateIds = (Account.keyToText . Account.accountKey) <$> accounts
    let stateAccountNumbers = Account.accountNumber <$> accounts
    return $ map Text.unpack $ filter (Text.isPrefixOf (Text.pack current)) $ configAliases <> stateAccountNumbers <> stateIds <> configIds
complete _ _ =
    return []

