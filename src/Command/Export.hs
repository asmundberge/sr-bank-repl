{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE NamedFieldPuns             #-}

module Command.Export
    ( accountConfigLookup
    , accountConfigsFromQuery
    , definition
    ) where

import Relude 

import AccountConfig (AccountConfig(..))
import ConfigFile (ConfigFile(..))
import Control.Monad.Trans.Except (throwE, withExceptT)
import Repl (Repl)
import ReplState (ReplState(..))
import System.FilePath ((</>), (<.>))
import Transaction (Transaction(..))
import qualified Api
import qualified Command
import qualified Data.ByteString.Lazy as BS
import qualified Data.Csv as Csv
import qualified Data.Text as Text
import qualified Data.Text.Read as Text
import qualified Data.Time.Calendar as Calendar
import qualified Repl
import qualified Transaction

definition :: Command.Definition
definition = Command.Definition
    { name = "export"
    , help = "<month> <account1> <accountN> - Export CSV for the preconfigured accounts"
    , command = command
    , complete = complete
    }

accountConfigLookup :: Text -> Repl [ AccountConfig ]
accountConfigLookup query = do
    (ConfigFile { accountConfigs }) <- Repl.readConfig
    let isAlias config = query == AccountConfig.alias config
    let isWildcard = query == "*"
    return $ filter (\c -> isAlias c || isWildcard) accountConfigs

parseMonth :: Text -> ExceptT Command.Error Repl (Integer, Int)
parseMonth month = do
    yyyy <- either (throwE . Command.ArgumentError . Text.pack . ("Could not parse year: " <>)) (return . fst) $ Text.decimal $ Text.takeWhile (/= '-') month
    mm <- either (throwE . Command.ArgumentError . Text.pack . ("Could not parse month: " <>)) (return . fst) $ Text.decimal $ Text.tail $ Text.dropWhile (/= '-') month
    return (yyyy, mm)

filterTransactions :: Integer -> Int -> [ Transaction ] -> [ Transaction ]
filterTransactions wantedYear wantedMonth transactions =
    filter (\(Transaction { date }) ->
        let
            (y, m, _) = Calendar.toGregorian (Transaction.getPlainDay date)
        in
        y == wantedYear && m == wantedMonth
        ) transactions

exportSingle :: (Integer, Int) -> AccountConfig -> ExceptT Command.Error Repl ()
exportSingle (yyyy, mm) (AccountConfig {..}) = do
    let path = directory </> show yyyy </> show mm <.> "csv"
    allTransactions <- withExceptT Command.ApiError $ Api.getTransactions accountKey
    let transactions = filterTransactions yyyy mm allTransactions
    putStrLn ("Writing file " <> path)
    liftIO $ BS.writeFile path (Csv.encodeDefaultOrderedByNameWith Csv.defaultEncodeOptions transactions)

command :: Command.Command
command args =
    case args of
        ( monthText : accountsText ) -> do
            (yyyy, mm) <- parseMonth monthText
            accountConfigs <- mconcat <$> mapM (lift . accountConfigLookup) accountsText
            _ <- mapM (exportSingle (yyyy, mm)) accountConfigs
            return Command.Continue

        _ ->
            throwE ( Command.ArgumentError "Usage: <month> <account1> <accountN>" )


-- COMPLETION

accountConfigsFromQuery :: Text -> Repl.ReplMonad [ AccountConfig ]
accountConfigsFromQuery query = do
    (ConfigFile { accountConfigs }) <- ask
    let isAliasPrefix config = query `Text.isPrefixOf` AccountConfig.alias config
    return $ filter isAliasPrefix accountConfigs


complete :: [ String ] -> String -> Repl.ReplMonad [ String ]
complete [] _ = do 
    (ReplState { today }) <- get
    let (yyyy, mm, _) = Calendar.toGregorian today
    return [ Text.unpack $ (Text.pack (show yyyy)) <> "-" <> (Text.justifyRight 2 '0' (Text.pack (show mm))) ]
complete _ current =
    fmap (Text.unpack . AccountConfig.alias) <$> accountConfigsFromQuery (Text.pack current)

