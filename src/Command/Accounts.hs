{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE NamedFieldPuns             #-}

module Command.Accounts
    ( definition
    ) where

import Relude

import Control.Monad.Trans.Except (throwE, withExceptT)
import qualified Account
import qualified Api
import qualified Command
import qualified Data.Text.IO as Text
import qualified Repl
import qualified ReplState

definition :: Command.Definition
definition = Command.Definition
    { name = "accounts"
    , help = "List available accounts"
    , command = command
    , complete = complete
    }

command :: Command.Command
command args =
    case args of
        [] -> do
            accounts <- withExceptT Command.ApiError $ Api.getAccounts
            lift $ Repl.modifyState (ReplState.setAccounts accounts)
            _ <- mapM (liftIO . Text.putStrLn . Account.pretty) accounts
            return Command.Continue

        _ ->
            throwE (Command.ArgumentError "Usage: accounts")


complete :: [ String ] -> String -> Repl.ReplMonad [ String ]
complete _ _ = return []
