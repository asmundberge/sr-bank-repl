{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}

module ConfigFile
    ( ConfigFile(..)
    ) where

import Relude

import AccountConfig (AccountConfig)
import Data.Aeson (FromJSON)

data ConfigFile = ConfigFile
    { clientId :: String
    , callbackUrl :: String
    , clientSecret :: Text
    , accountConfigs :: [ AccountConfig ]
    }
    deriving (Show, Generic)

instance FromJSON ConfigFile
