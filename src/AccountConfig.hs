{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}

module AccountConfig
    ( AccountConfig(..)
    ) where

import Relude

import qualified Account
import Data.Aeson (FromJSON)

data AccountConfig = AccountConfig
    { accountKey :: Account.Key
    , directory :: FilePath
    , alias :: Text
    }
    deriving (Show, Generic)

instance FromJSON AccountConfig
