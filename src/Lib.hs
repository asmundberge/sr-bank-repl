{-# LANGUAGE NamedFieldPuns             #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}

module Lib
    ( srbank
    ) where

import Relude

import Command (Command)
import Repl (Repl, ReplMonad)
import qualified Api
import qualified Auth.File
import qualified CliOptions
import qualified Command
import qualified Command.Accounts
import qualified Command.Export
import qualified Command.Transactions
import qualified Completion
import qualified Data.Text as Text
import qualified Data.Yaml as Yaml
import qualified Options.Applicative as Options
import qualified Repl
import qualified ReplState
import qualified System.Console.Haskeline as Haskeline

data Error 
    = ApiError Api.Error
    | MonthError String
    deriving (Show)

commandDefs :: [ Command.Definition ]
commandDefs =
    [ Command.Accounts.definition
    , Command.Transactions.definition
    , Command.Export.definition
    ]

parseCommand :: Maybe Text -> (Command, [ Text ])
parseCommand cmd =
    let 
        (commandName : arguments) = maybe [ "quit" ] Text.words cmd
    in
    case commandName of
        "quit"  ->
            (Command.quit, [])
            
        "help" ->
            (Command.printHelp commandDefs, [])

        _ -> 
            (Command.lookup commandName commandDefs, arguments)

executeCommand :: Maybe Text -> Repl Command.Result
executeCommand maybeCmd = do
    let (cmd, args) = parseCommand maybeCmd
    Command.run cmd args


repl :: Repl ()
repl = do
    cmdInput <- Haskeline.getInputLine "> "
    result <- executeCommand (Text.pack <$> cmdInput)
    case result of
        Command.Quit -> return ()
        Command.Continue -> repl

replSettings :: Haskeline.Settings ReplMonad
replSettings =
    Haskeline.defaultSettings 
        & Haskeline.setComplete (Completion.completionFunc commandDefs)

optParseOptions :: Options.ParserInfo CliOptions.CliOptions
optParseOptions = Options.info (CliOptions.parser <**> Options.helper)
    ( Options.fullDesc
      <> Options.progDesc "A command-line utility to fetch data from the Sparebank 1 API"
      <> Options.header "SR-Bank REPL" )

srbank :: IO ()
srbank = do
    cliOpts <- Options.execParser optParseOptions
    config <- Yaml.decodeFileThrow "srbank.yaml"
    authToken <- Auth.File.read
    iState <- ReplState.initialState authToken

    case cliOpts of
        CliOptions.Command cmdOption -> 
            Repl.runRepl replSettings config iState $ void $ executeCommand (Just cmdOption)

        CliOptions.Empty -> do
            putStrLn "Welcome to the SR-Bank repl. Ctrl-D to exit"

            Repl.runRepl replSettings config iState repl

