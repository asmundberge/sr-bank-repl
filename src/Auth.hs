{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE NamedFieldPuns    #-}

module Auth 
    ( Error
    , sendRequest
    ) where

import Relude

import Auth.Types (AuthToken(..), AuthCode, addAuthHeader)
import ConfigFile (ConfigFile(..))
import Control.Monad.Trans.Except (catchE, throwE)
import Data.Aeson (FromJSON)
import Repl (Repl, readConfig, getState, modifyState)
import ReplState (ReplState(..), setToken)
import Web.Browser (openBrowser)
import qualified Auth.File
import qualified Data.ByteString.Char8 as BS
import qualified Data.Text.Encoding as Text
import qualified Network.HTTP.Simple as Http
import qualified Network.URI.Encode as Uri
import qualified System.IO

data Error
    = Unauthenticated
    | ParseError Http.JSONException
    | OtherError
    deriving (Show)

makeAuthUrl :: ExceptT Error Repl String
makeAuthUrl = do
    (ConfigFile { clientId, callbackUrl }) <- lift Repl.readConfig
    return $ mconcat [ "https://api-auth.sparebank1.no/oauth/authorize"
                     , "?finInst=fid-sr-bank"
                     , "&client_id="
                     , clientId
                     , "&state=0"
                     , "&redirect_uri="
                     , Uri.encode callbackUrl
                     , "&response_type=code"
                     ]

getAuthCode :: ExceptT Error Repl AuthCode
getAuthCode = do
    url <- makeAuthUrl
    _ <- liftIO $ openBrowser url
    putStrLn "The authorization process has been started in your browser."
    putStrLn ""
    putStrLn "Please go to your browser and complete the process. You should be presented with an authorization code. Paste it in here."
    putStr "Code: " >> liftIO (System.IO.hFlush System.IO.stdout) >> getLine

mapError :: Http.Response (Either Http.JSONException a) -> ExceptT Error Repl a
mapError r =
    case (Http.getResponseStatusCode r, Http.getResponseBody r) of
        (200, Right a) -> return a
        (401, _) -> throwE Unauthenticated
        (_, Left e) -> throwE (ParseError e)
        _ -> throwE OtherError

getToken :: ExceptT Error Repl AuthToken
getToken = do
    let wrongCode = putStrLn "The request failed. Let's create a new code"
    token <- getAuthCode >>= getTokenFromCode
        & (flip catchE) (const (wrongCode >> getAuthCode >>= getTokenFromCode))
    lift $ Repl.modifyState (ReplState.setToken (Just token)) 
    liftIO $ Auth.File.write token
    return token

getTokenFromCode :: AuthCode -> ExceptT Error Repl AuthToken
getTokenFromCode authCode = do
    (ConfigFile { clientId, clientSecret, callbackUrl }) <- lift Repl.readConfig
    let req = Http.parseRequest "https://api-auth.sparebank1.no/oauth/token"
    let postReq = fmap (Http.setRequestMethod "POST") req
    let reqWithForm = fmap (Http.setRequestBodyURLEncoded
               [ ( "client_id", BS.pack clientId )
               , ( "client_secret", Text.encodeUtf8 clientSecret )
               , ( "redirect_uri", BS.pack callbackUrl )
               , ( "grant_type", "authorization_code" )
               , ( "code", Text.encodeUtf8 authCode )
               , ( "state", "0" )
               ]) postReq
    Http.httpJSONEither (maybe Http.defaultRequest id reqWithForm) >>= mapError

sendRequest :: (FromJSON a) => Http.Request -> ExceptT Error Repl a
sendRequest req = sendRequest' False req

sendRequest' :: (FromJSON a) => Bool -> Http.Request -> ExceptT Error Repl a
sendRequest' secondAttempt req = do
    (ReplState { authToken }) <- lift Repl.getState
    token <- maybe getToken return authToken
    let req' = addAuthHeader token req
    let req'' = Http.addRequestHeader "Accept" "application/vnd.sparebank1.v1+json;charset=utf-8" req'
    let res = Http.httpJSONEither req'' >>= mapError
    if secondAttempt then
      res
    else
      res `catchE` (refreshTokenAndRepeat token req)

refreshTokenAndRepeat token req error =
    case error of
      Unauthenticated -> do
          newToken <- getTokenFromRefreshToken token
          lift $ Repl.modifyState (\s -> s { authToken = Just newToken })
          liftIO $ Auth.File.write newToken
          sendRequest' True req
      _ -> throwE error

getTokenFromRefreshToken :: AuthToken -> ExceptT Error Repl AuthToken
getTokenFromRefreshToken (AuthToken { refresh_token }) = do
    (ConfigFile { clientId, clientSecret, callbackUrl }) <- lift Repl.readConfig
    let req = Http.parseRequest "https://api-auth.sparebank1.no/oauth/token"
    let postReq = fmap (Http.setRequestMethod "POST") req
    let reqWithForm = fmap (Http.setRequestBodyURLEncoded
               [ ( "client_id", BS.pack clientId )
               , ( "client_secret", Text.encodeUtf8 clientSecret )
               , ( "refresh_token", Text.encodeUtf8 refresh_token )
               , ( "grant_type", "refresh_token" )
               ]) postReq
    Http.httpJSONEither (maybe Http.defaultRequest id reqWithForm) >>= mapError
