{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Api
    ( Error
    , getAccounts
    , getTransactions
    ) where

import Relude

import Account (Account)
import Transaction (Transaction)
import Control.Monad.Trans.Except (throwE, withExceptT)
import Data.Aeson (FromJSON)
import Repl (Repl)
import qualified Account
import qualified Auth
import qualified Data.Text as Text
import qualified Network.HTTP.Simple as Http

data Error 
    = UrlError String
    | AuthError Auth.Error
    deriving (Show)

apiUrl :: String -> String
apiUrl path = "https://api.sparebank1.no/personal/banking" <> path

makeRequest :: String -> ExceptT Error Repl Http.Request
makeRequest path = do
    let url = apiUrl path
    maybe (throwE (UrlError url)) return $ Http.parseRequest url

data AccountsDto = AccountsDto
    { accounts :: [ Account ]
    }
    deriving (Show, Generic)

instance FromJSON AccountsDto

getAccounts :: ExceptT Error Repl [Account]
getAccounts = do
    req <- makeRequest "/accounts" 
    accountsDto <- withExceptT AuthError $ Auth.sendRequest req
    return (Api.accounts accountsDto)

data TransactionsDto = TransactionsDto
    { transactions :: [ Transaction ]
    }
    deriving (Show, Generic)

instance FromJSON TransactionsDto

getTransactions :: Account.Key -> ExceptT Error Repl [ Transaction ]
getTransactions accountKey = do
    req <- makeRequest (Text.unpack ("/transactions?accountKey=" <> Account.keyToText accountKey))
    transactionsDto <- withExceptT AuthError $ Auth.sendRequest req
    return (Api.transactions transactionsDto)
